module LIB_MeshInfo
    use LIB_WriteToENS, only : iFindStringInFile, GetUnit
    implicit none

    ! important
    integer(4)                  :: iDofT
    integer(4)                  :: Ndim
    integer(4)                  :: NodT
    integer(4)                  :: NelT

    integer(4)                  :: NGROUPS      ! typically set to 1 when GENERIC GROUP is used


    ! LIB_WriteToVTK variables
    real(8),    allocatable :: x_uns(:)            !dimension Nn
    real(8),    allocatable :: y_uns(:)            !dimension Nn
    real(8),    allocatable :: z_uns(:)            !dimension Nn

    integer(4), allocatable :: connect(:)

    integer(4), allocatable :: IE(:), JE(:), cell_type(:)   !
    real(8),    allocatable :: solution(:,:)                ! nodal values
    real(8),    allocatable :: cell_solution(:,:)           ! element values

    integer(4), allocatable :: cellId(:)                    ! elements cell ID


    ! writting units
    integer(4) Unit_MESH, Unit_DataOUT, Unit_BASPARAM



    ! WriteToVTK V3.0 additionals
    logical                    :: flag_V3, flag_V2
    integer(4)                 :: nPhyGroups
    character(30), allocatable :: phyGroupName(:)   ! physical group names
    integer(4),    allocatable :: phyGroupEl(:)     ! physical group elements number
    character(30), allocatable :: phyGroupType(:)   ! physical group type ('Tetra' 'Tri')
    integer(4),    allocatable :: phyGroupNode(:)   ! physical group nodes


    ! WriteToVTK V3.2
    ! generates groups for additional nodes
    integer(4)                 :: nPhyGroupsAdd        ! groups with additional nodes. +1 if phyGroupNode > phyGroupType
    character(30), allocatable :: phyGroupNameAdd(:)   ! physical group names. Header name will be taken from original group name
    integer(4),    allocatable :: phyGroupElAdd(:)     ! physical group elements number
    character(30), allocatable :: phyGroupTypeAdd(:)   ! physical group type ('Point' 'Tetra' 'Tri')
    integer(4),    allocatable :: phyGroupNodeAdd(:)   ! physical group nodes


    ! for group modifications
    integer(4), allocatable    :: IE2(:), JE2(:), cell_type2(:)


    ! WriteToVTK V3.1 additionals
    integer(4)                    :: sizeDOFe
    character(30), allocatable    :: DOFeNames(:)

    integer(4)                    :: DofEls, DofElv     ! numero de parametros elementales vectoriales y escalares
    integer, allocatable          :: PointerDofEls(:), PointerDofElv(:)

    ! flags
    logical                     :: flag_ElementOutput
    logical                     :: flag_additionalNodes


    CONTAINS


    function readMeshInfo() result(E_IO)
    !------------------------------------------------------------------
    ! read mesh info fron tag
    !
    !------------------------------------------------------------------
    implicit none
    integer(4) E_IO

    integer(4) i

    ! reading number of groups
    rewind(unit_BASPARAM)
    E_IO = iFindStringInFile('*ElementLibraryControl',unit_BASPARAM)
    read(Unit_BASPARAM,*, iostat = E_IO) nPhyGroups

    ! allocating arrays
    allocate(phyGroupName(nPhyGroups))
    allocate(phyGroupEl  (nPhyGroups))
    allocate(phyGroupType(nPhyGroups))
    allocate(phyGroupNode(nPhyGroups))

    ! reading arrays, new in version v3.0
    rewind(Unit_BASPARAM)
    E_IO = iFindStringInFile('*WriteToVTK',Unit_BASPARAM)
    read(Unit_BASPARAM,*, iostat = E_IO) ( phyGroupName(i), phyGroupEl(i), phyGroupType(i), phyGroupNode(i) , i=1 , nPhyGroups )

    ! reading Element fields, new in version v3.1
    rewind(Unit_BASPARAM)
    E_IO = iFindStringInFile('*ElementWriteToVTK',Unit_BASPARAM)
    if (E_IO.eq.0) then
        read(Unit_BASPARAM,*, iostat = E_IO) sizeDOFe
        allocate(DOFeNames(sizeDOFe))
        read(Unit_BASPARAM,*, iostat = E_IO) DOFeNames
    end if

    end function


    FUNCTION select_nodel(name) result(nodes)
        IMPLICIT NONE
        CHARACTER(LEN=*), INTENT(IN)     :: name
!        CHARACTER, INTENT(IN)            :: name(:)
        INTEGER                          :: nodes

        INTEGER :: leng

        nodes = 0

        leng = LEN(TRIM(name))

        IF ( name(1:leng) == "Line2"  ) nodes=2   !     2-node line.
        IF ( name(1:leng) == "Tri3"   ) nodes=3   !     3-node triangle.
        IF ( name(1:leng) == "Quad4"  ) nodes=4   !     4-node quadrangle.
        IF ( name(1:leng) == "Tetra4" ) nodes=4   !     4-node tetrahedron.
        IF ( name(1:leng) == "Hexa8"  ) nodes=8   !     8-node hexahedron.
        IF ( name(1:leng) == "Prism6" ) nodes=6   !     6-node prism.
        IF ( name(1:leng) == "Pyram5" ) nodes=5   !     5-node pyramid.
        IF ( name(1:leng) == "Line3"  ) nodes=3   !     3-node second order line (2 nodes associated with the vertices and 1 with the edge).
        IF ( name(1:leng) == "Tri6"   ) nodes=6   !     6-node second order triangle (3 nodes associated with the vertices and 3 with the edges).
        IF ( name(1:leng) == "Quad9"  ) nodes=9   !     9-node second order quadrangle (4 nodes associated with the vertices, 4 with the edges and 1 with the face).
        IF ( name(1:leng) == "Tetra10") nodes=10  !    10-node second order tetrahedron (4 nodes associated with the vertices and 6 with the edges).
        IF ( name(1:leng) == "Hexa27" ) nodes=27  !    27-node second order hexahedron (8 nodes associated with the vertices, 12 with the edges, 6 with the faces and 1 with the volume).
        IF ( name(1:leng) == "Prism18") nodes=18  !    18-node second order prism (6 nodes associated with the vertices, 9 with the edges and 3 with the quadrangular faces).
        IF ( name(1:leng) == "Pyram14") nodes=14  !    14-node second order pyramid (5 nodes associated with the vertices, 8 with the edges and 1 with the quadrangular face).
        IF ( name(1:leng) == "Point1" ) nodes=1   !     1-node point.
        IF ( name(1:leng) == "Quad8"  ) nodes=8   !     8-node second order quadrangle (4 nodes associated with the vertices and 4 with the edges).
        IF ( name(1:leng) == "Hexa20" ) nodes=20  !    20-node second order hexahedron (8 nodes associated with the vertices and 12 with the edges).
        IF ( name(1:leng) == "Prism15") nodes=15  !    15-node second order prism (6 nodes associated with the vertices and 9 with the edges).
        IF ( name(1:leng) == "Pyram14") nodes=13  !    13-node second order pyramid (5 nodes associated with the vertices and 8 with the edges).
        IF ( name(1:leng) == "Tri9"   ) nodes=9   !     9-node third order incomplete triangle (3 nodes associated with the vertices, 6 with the edges)
        IF ( name(1:leng) == "Tri10"  ) nodes=10  !    10-node third order triangle (3 nodes associated with the vertices, 6 with the edges, 1 with the face)
        IF ( name(1:leng) == "Tri12"  ) nodes=12  !    12-node fourth order incomplete triangle (3 nodes associated with the vertices, 9 with the edges)
        IF ( name(1:leng) == "Tri15"  ) nodes=15  !    15-node fourth order triangle (3 nodes associated with the vertices, 9 with the edges, 3 with the face)
        IF ( name(1:leng) == "Tri15i" ) nodes=15  !    15-node fifth order incomplete triangle (3 nodes associated with the vertices, 12 with the edges)
        IF ( name(1:leng) == "Tri21"  ) nodes=21  !    21-node fifth order complete triangle (3 nodes associated with the vertices, 12 with the edges, 6 with the face)
        IF ( name(1:leng) == "Edge4"  ) nodes=4   !     4-node third order edge (2 nodes associated with the vertices, 2 internal to the edge)
        IF ( name(1:leng) == "Edge5"  ) nodes=5   !     5-node fourth order edge (2 nodes associated with the vertices, 3 internal to the edge)
        IF ( name(1:leng) == "Edge6"  ) nodes=6   !     6-node fifth order edge (2 nodes associated with the vertices, 4 internal to the edge)
        IF ( name(1:leng) == "Tetra20") nodes=20  !    20-node third order tetrahedron (4 nodes associated with the vertices, 12 with the edges, 4 with the faces)
        IF ( name(1:leng) == "Tetra35") nodes=35  !    35-node fourth order tetrahedron (4 nodes associated with the vertices, 18 with the edges, 12 with the faces, 1 in the volume)
        IF ( name(1:leng) == "Tetra56") nodes=56  !    56-node fifth order tetrahedron (4 nodes associated with the vertices, 24 with the edges, 24 with the faces, 4 in the volume)
        IF ( name(1:leng) == "Hexa64" ) nodes=64  !    64-node third order hexahedron (8 nodes associated with the vertices, 24 with the edges, 24 with the faces, 8 in the volume)
        IF ( name(1:leng) == "Hexa125") nodes=125 !   125-node fourth order hexahedron (8 nodes associated with the vertices, 36 with the edges, 54 with the faces, 27 in the volume)

    END FUNCTION


    FUNCTION select_cellType(name) result(cellTypeVTK)
    !------------------------------------------------------------------
    ! function that maps GMSH element names to VTK legacy format code
    !
    !------------------------------------------------------------------
    IMPLICIT NONE
    CHARACTER(LEN=*), INTENT(IN)     :: name
    INTEGER(4)                       :: cellTypeVTK

    INTEGER :: leng

    leng = LEN(TRIM(name))
    
    IF ( name(1:leng) == "Line2"  ) cellTypeVTK = 3     !     2-node line
    IF ( name(1:leng) == "Tri3"   ) cellTypeVTK = 5     !     3-node triangle.    
    IF ( name(1:leng) == "Tri6"   ) cellTypeVTK = 22    !     6-node second order triangle (3 nodes associated with the vertices and 3 with the edges).
    IF ( name(1:leng) == "Quad4"  ) cellTypeVTK = 9     !     4-node quadrilateral.
    IF ( name(1:leng) == "Tetra4" ) cellTypeVTK = 10    !     4-node tetrahedron.
    IF ( name(1:leng) == "Tetra10") cellTypeVTK = 24    !    10-node second order tetrahedron (4 nodes associated with the vertices and 6 with the edges).
    IF ( name(1:leng) == "Point1" ) cellTypeVTK = 1     !     1-node point.

    END FUNCTION

    subroutine gmsh2ens(gmsh_cell, ens_cell)
    !------------------------------------------------------------------
    ! workoround for gmsh2ens below function
    !
    !------------------------------------------------------------------
    IMPLICIT NONE
    CHARACTER(LEN=*), INTENT(IN)     :: gmsh_cell
    character(len=*), INTENT(OUT)    :: ens_cell

    INTEGER :: leng

    leng = LEN(TRIM(gmsh_cell))


    IF ( gmsh_cell(1:leng) == "Line2"  ) ens_cell = "bar2"     !     2-node line
    IF ( gmsh_cell(1:leng) == "Tri3"   ) ens_cell = "tria3"    !     3-node triangle.
    IF ( gmsh_cell(1:leng) == "Tri6"   ) ens_cell = "tria6"    !     6-node second order triangle (3 nodes associated with the vertices and 3 with the edges).
    IF ( gmsh_cell(1:leng) == "Quad4"  ) ens_cell = "quad4"    !     4-node quadrilateral.
    IF ( gmsh_cell(1:leng) == "Tetra4" ) ens_cell = "tetra4"   !     4-node tetrahedron.
    IF ( gmsh_cell(1:leng) == "Tetra10") ens_cell = "tetra10"  !    10-node second order tetrahedron (4 nodes associated with the vertices and 6 with the edges).
    IF ( gmsh_cell(1:leng) == "Point1" ) ens_cell = "point"    !     1-node point.

    end subroutine

!    FUNCTION gmsh2ens_bis(gmsh_cell) result(ens_cell)
!    !------------------------------------------------------------------
!    ! function that maps GMSH element names into ENSIGHT cell names
!    !
!    !------------------------------------------------------------------
!    IMPLICIT NONE
!    CHARACTER(LEN=:), INTENT(IN)     :: gmsh_cell
!    character(len=:)                :: ens_cell
!
!    INTEGER :: leng
!
!    leng = LEN(TRIM(gmsh_cell))
!
!
!    IF ( gmsh_cell(1:leng) == "Line2"  ) ens_cell = "bar2"     !     2-node line
!    IF ( gmsh_cell(1:leng) == "Tri3"   ) ens_cell = "tria3"    !     3-node triangle.
!    IF ( gmsh_cell(1:leng) == "Tri6"   ) ens_cell = "tria6"    !     6-node second order triangle (3 nodes associated with the vertices and 3 with the edges).
!    IF ( gmsh_cell(1:leng) == "Quad4"  ) ens_cell = "quad4"    !     4-node quadrilateral.
!    IF ( gmsh_cell(1:leng) == "Tetra4" ) ens_cell = "tetra4"   !     4-node tetrahedron.
!    IF ( gmsh_cell(1:leng) == "Tetra10") ens_cell = "tetra10"  !    10-node second order tetrahedron (4 nodes associated with the vertices and 6 with the edges).
!    IF ( gmsh_cell(1:leng) == "Point1" ) ens_cell = "point"    !     1-node point.
!
!
!    END FUNCTION




    FUNCTION getCellId() result(E_IO)
    !------------------------------------------------------------------
    ! Computes CellId element array.
    !
    ! Beware: This function must be called after getIncidence function
    ! and IE reallocation because uses the new IE size.
    !
    !------------------------------------------------------------------
    implicit none

    integer(4)      :: E_IO

    integer(4)      :: i
    integer(4)      :: iStart, iEnd         ! Bounds for cellId array

    integer(4)      :: nodes

    allocate(cellId(size(IE)))


    iStart = 1
    iEnd   = 0
    do i=1, nPhyGroups

        if (phyGroupName(i)(1:1).eq.'!') cycle

        iEnd = iEnd + phyGroupEl(i)

        cellId(iStart:iEnd) = i

        iStart = iEnd + 1
    end do

    ! groups ID for groups with additional nodes
    nPhyGroupsAdd = 0
    do i=1, nPhyGroups

        if (phyGroupName(i)(1:1).eq.'!') cycle

        nodes = select_nodel(phyGroupType(i))
        if (phyGroupNode(i) > nodes) then
            nPhyGroupsAdd = nPhyGroupsAdd + 1

            iEnd = iEnd + phyGroupElAdd(nPhyGroupsAdd)
            cellId(iStart:iEnd) = -i
            iStart = iEnd + 1

        end if
    end do


    E_IO = 1

    END FUNCTION



end module LIB_MeshInfo

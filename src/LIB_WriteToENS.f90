module LIB_WriteToENS
    implicit none
    private

    public :: ENS_INI_GEO
    public :: ENS_GEO_UNST
    public :: ENS_CON
    public :: ENS_END_GEO

    public :: ENS_VAR_SCAL
    public :: ENS_VAR_VEC

    public :: iFindStringInFile
    public :: GetUnit
    public :: Upper_Case

    ! real and integer precision definition
    integer, parameter:: R8P  = selected_real_kind(15,307)  ! 15  digits, range $[\pm 10^{-307}~~ ,\pm 10^{+307}~~  -1]$
    integer, parameter:: I4P  = selected_int_kind(9)        ! range $[-2^{31} ,+2^{31}  -1]$

    ! ENSIGHT FORMAT
    character(5), parameter:: FI  = '(I10)'               ! Integer  output format
    character(7), parameter:: FR  = '(E12.5)'             ! Real  output format


    integer(I4P) :: unit_GEO


    logical :: f_out_ascii = .TRUE.

    contains

    function ENS_INI_GEO(filename) result(ierr)
    !---------------------------------------------------------
    ! Function for writting header at geo file.
    !---------------------------------------------------------
    implicit none
    character(*), intent(in) :: filename
    integer(I4P)                   :: ierr

    call system('mkdir -p ensightData')

    unit_GEO = GetUnit()
    open(unit   = unit_GEO,                       &
       file     = 'ensightData/'//TRIM(filename), &
       form     = 'FORMATTED',                    &
       access   = 'SEQUENTIAL',                   &
       action   = 'WRITE',                        &
       iostat   = ierr)

    ! writing header of file
    write(unit_GEO,'(A)',iostat=ierr) 'Exported Ensight Model Geometry File by WriteTOENS'
    write(unit_GEO,'(A)',iostat=ierr) 'Ensight 7.6'
    write(unit_GEO,'(A)',iostat=ierr) 'node id assign'
    write(unit_GEO,'(A)',iostat=ierr) 'element id assign'

    end function

    function ENS_GEO_UNST(NN,X,Y,Z) result(ierr)
    !---------------------------------------------------------
    ! This function must be used after ENS_INI_GEO function
    ! Function for saving mesh coordinates.
    !---------------------------------------------------------
    implicit none
    !--------------------------------------------------------------------------------------------------------------------------------
    integer(I4P), intent(IN):: NN        ! number of nodes
    real(R8P),    intent(IN):: X(NN)     ! x coordinates of all nodes
    real(R8P),    intent(IN):: Y(NN)     ! y coordinates of all nodes
    real(R8P),    intent(IN):: Z(NN)     ! z coordinates of all nodes
    integer(I4P)            :: ierr      ! Input/Output inquiring flag: $0$ if IO is done, $> 0$ if IO is not done

    write(unit_GEO,'(A)',iostat=ierr) 'part'
    write(unit_GEO,FI,   iostat=ierr) 1
    write(unit_GEO,'(A)',iostat=ierr) 'GENERIC'
    write(unit_GEO,'(A)',iostat=ierr) 'coordinates'
    write(unit_GEO,FI,   iostat=ierr) NN
    write(unit_GEO,FR,   iostat=ierr) X
    write(unit_GEO,FR,   iostat=ierr) Y
    write(unit_GEO,FR,   iostat=ierr) Z

    end function

    function ENS_CON(NC,NodEl,NodG,JE,groupType) result(ierr)
    !---------------------------------------------------------
    ! This function must be used after ENS_GEO_UNST Function.
    ! Function for saving mesh connectivity.
    ! Could be used recursively for each physical group.
    !---------------------------------------------------------
    implicit none
    integer(I4P), intent(IN):: NC              ! number of cells
    integer(I4P), intent(IN):: NodEl           ! number of nodes per cell
    integer(I4P), intent(IN):: NodG            ! number of geometric nodes per cell
    integer(I4P), intent(IN):: JE(*)           ! mesh connectivity
    character(len=*), intent(IN) :: groupType ! Ensight cell name
    integer(I4P)            :: ierr

    ! internal variables
    integer(I4P) :: iElem, iNod, ipElem, ipNod, i


    ! check groupType must be a compatible one!
    write(unit_GEO,'(A)',iostat=ierr) groupType
    write(unit_GEO,FI,   iostat=ierr) NC
    do iElem=1, NC
        ipElem = (iElem-1)*NodEl
        !write(unit_GEO,FI,iostat=ierr) (JE(ipElem+i), i=1, NodG)
        do iNod=1, NodG
            ipNod = ipElem + iNod
            write(unit_GEO,FI,advance='no',iostat=ierr) JE(ipNod)
        enddo
        write(unit_GEO,*)
    enddo

    end function

    function ENS_END_GEO() result(ierr)
    !---------------------------------------------------------
    ! Function to finalizes GEOMETRY file write.
    ! Must be use somewhere after ENS_CON function to release
    ! the unit_GEO unit.
    !---------------------------------------------------------
    implicit none
    integer(I4P) :: ierr

    close(unit=unit_GEO, iostat=ierr)

    end function

    function ENS_VAR_VEC(NC_NN,varname,varX,varY,varZ,filename) result(ierr)
    !---------------------------------------------------------
    ! Function for saving field of vector variable (R8P).
    !---------------------------------------------------------
    implicit none
    integer(I4P), intent(IN):: NC_NN        ! number of nodes or cells
    character(*), intent(IN):: varname      ! variable name
    real(R8P),    intent(IN):: varX(NC_NN)  ! variable to be saved
    real(R8P),    intent(IN):: varY(NC_NN)  ! variable to be saved
    real(R8P),    intent(IN):: varZ(NC_NN)  ! variable to be saved
    character(*), intent(IN):: filename     ! output filename
    integer(I4P)::             ierr         ! Input/Output inquiring flag: $0$ if IO is done, $> 0$ if IO is not done

    ! internal variables
    integer(I4P)       :: freeUnit

    freeUnit = GetUnit()

    if (f_out_ascii) then

        open(unit   = freeUnit,       &
           file     = TRIM(filename), &
           form     = 'FORMATTED',    &
           access   = 'SEQUENTIAL',   &
           action   = 'WRITE',        &
           iostat   = ierr)

        write(freeUnit,'(A)', iostat=ierr) varname
        write(freeUnit,'(A)', iostat=ierr) 'part'
        write(freeUnit,FI,    iostat=ierr) 1                 ! only one part (as Generic)
        write(freeUnit,'(A)', iostat=ierr) 'coordinates'
        write(freeUnit,FR,    iostat=ierr) varX
        write(freeUnit,FR,    iostat=ierr) varY
        write(freeUnit,FR,    iostat=ierr) varZ

        close(freeUnit)

    endif

    end function


    function ENS_VAR_SCAL(NC_NN,varname,var,filename) result(ierr)
    !---------------------------------------------------------
    ! Function for saving field of scalar variable (R8P).
    !---------------------------------------------------------
    implicit none
    integer(I4P), intent(IN):: NC_NN        ! number of nodes or cells
    character(*), intent(IN):: varname      ! variable name
    real(R8P),    intent(IN):: var(NC_NN)   ! variable to be saved
    character(*), intent(IN):: filename
    integer(I4P)::             ierr         ! Input/Output inquiring flag: $0$ if IO is done, $> 0$ if IO is not done

    ! internal variables
    integer(I4P)       :: freeUnit

    freeUnit = GetUnit()

    if (f_out_ascii) then

        open(unit   = freeUnit,       &
           file     = TRIM(filename), &
           form     = 'FORMATTED',    &
           access   = 'SEQUENTIAL',   &
           action   = 'WRITE',        &
           iostat   = ierr)

        write(freeUnit,'(A)', iostat=ierr) varname
        write(freeUnit,'(A)', iostat=ierr) 'part'
        write(freeUnit,FI,    iostat=ierr) 1                 ! only one part (as Generic)
        write(freeUnit,'(A)', iostat=ierr) 'coordinates'
        write(freeUnit,FR,    iostat=ierr) var

        close(freeUnit)

    endif

    end function



    function GetUnit() result(Free_Unit)
    !--------------------------------------------------------------------------------------------------------------------------------
    !!The GetUnit function is used for getting a free logic unit. The users of \LIBVTKIO does not know which is
    !!the logical unit: \LIBVTKIO handels this information without boring the users. The logical unit used is safe-free: if the
    !!program calling \LIBVTKIO has others logical units used \LIBVTKIO will never use these units, but will choice one that is free.
    !--------------------------------------------------------------------------------------------------------------------------------

    implicit none

    !--------------------------------------------------------------------------------------------------------------------------------
    integer(I4P):: Free_Unit ! free logic unit
    integer(I4P):: n1        ! counter
    integer(I4P):: ios       ! inquiring flag
    logical(4)::   lopen     ! inquiring flag
    !--------------------------------------------------------------------------------------------------------------------------------

    !--------------------------------------------------------------------------------------------------------------------------------
    !!The following is the code snippet of GetUnit function: the units 0, 5, 6, 9 and all non-free units are discarded.
    !!
    !(\doc)codesnippet
    Free_Unit = -1_I4P                                      ! initializing free logic unit
    n1=1_I4P                                                ! initializing counter
    do
      if ((n1/=5_I4P).AND.(n1/=6_I4P).AND.(n1/=9_I4P)) then
        inquire (unit=n1,opened=lopen,iostat=ios)           ! verify logic units
        if (ios==0_I4P) then
          if (.NOT.lopen) then
            Free_Unit = n1                                  ! assignment of free logic
            return
          endif
        endif
      endif
      n1=n1+1_I4P                                           ! updating counter
    enddo
    return
    !!GetUnit function is private and cannot be called outside \LIBVTKIO. If you are interested to use it change its scope to public.
    !--------------------------------------------------------------------------------------------------------------------------------
    end function GetUnit

    FUNCTION iFindStringInFile(Str, ioUnit) RESULT (iError)
        ! -----
        ! Busca un String en un archivo (STR), sino lo encuentra rebovina el archivo
        ! y pone iError < 0 como indicador de no hallazgo
        ! Str: String to find, ioUnit: Unit assigned to Input File; iError: Error Status variable
        ! -----
        IMPLICIT NONE
        ! Parameters
        LOGICAL,PARAMETER  :: Segui=.True.
        ! Arguments
        CHARACTER(*), INTENT(IN) :: Str
        INTEGER, INTENT (IN) :: ioUnit
        ! Locals
        CHARACTER(LEN=120) :: DummyString
        CHARACTER(LEN=120) :: upStr
        INTEGER :: iError
        INTEGER :: Leng
        ! -----

        iError=0
        Leng = LEN_TRIM(Str)
        upStr = Upper_Case(Str)       ! Line added by NB. Converts Str to Upper Case string
        ! ---
        REWIND(ioUnit)
        Search_Loop: DO WHILE (segui)
            READ(ioUnit,'(1A120)',IOSTAT=iError) DummyString ! iError es 0 si lee con exito, >0 si hay error y <0 si es end of file.
            DummyString = Upper_Case(DummyString)   ! line added by NB
            !       if (iError==59) backspace(ioUnit)
            IF (iError.lt.0) THEN
                REWIND (ioUnit)
                EXIT Search_Loop
            END IF
            IF ( DummyString(1:1)    /=    '*'      ) CYCLE Search_Loop
            IF ( DummyString(1:Leng) == upStr(1:Leng) ) EXIT Search_Loop
        END DO Search_Loop

    END FUNCTION iFindStringInFile

    FUNCTION Upper_Case(string)
        ! -----
        ! The Upper_Case function converts the lower case characters of a string to upper case one.
        ! Use this function in order to achieve case-insensitive: all character variables used
        ! are pre-processed by Uppper_Case function before these variables are used. So the users
        ! can call functions without pey attention of case of the keywords passed to the functions.
        ! -----
        IMPLICIT NONE
        ! -----
        CHARACTER(LEN=*), INTENT(IN):: string     ! string to be converted
        CHARACTER(LEN=LEN(string))::   Upper_Case ! converted string
        INTEGER::                      n1         ! characters counter
        ! -----

        Upper_Case = string
        DO n1=1,LEN(string)
            SELECT CASE(ICHAR(string(n1:n1)))
                CASE(97:122)
                    Upper_Case(n1:n1)=CHAR(ICHAR(string(n1:n1))-32) ! Upper case conversion
            END SELECT
        ENDDO

        ! -----
    END FUNCTION Upper_Case

end module LIB_WriteToENS

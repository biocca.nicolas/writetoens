program WriteToENS
    use LIB_WriteToENS
    use LIB_MeshInfo
    implicit none

    !--------------------------------------------------------------------------------------------------------------------------------

    integer(4) ::  E_IO

    integer(4),    allocatable :: IDGroup(:), NelTG(:)
    character(30), allocatable :: NelName(:), NodElG(:), DOFNames(:)
    integer(4) iError, i, j, k


    integer(4) LastChar
    integer(4) DOFs, DOFv
    integer, allocatable :: PointerDOFs(:), PointerDOFv(:)
    character (len=30) FieldName


    CHARACTER (LEN=21) :: ArchiveNameINPUT, ArchiveNameOUPUT


    integer(4) initial_count, final_count, step

    character (len=10) :: arg
    integer(4) Narg
    integer(4) Nlist
    integer(4), allocatable :: listDOF(:)

    ! WIP: global iters
    !
    logical :: printGlobarIter                          ! flag to activate printting global iters
    CHARACTER (LEN=60):: ArchiveNameGi, ArchiveNameGo   ! input/output name for global iters
    integer(4) nStep                                    ! time step to print global iters
    integer(4) Nconverg                                 ! counter

    logical :: foundFile                                ! TRUE is file exists


    integer(4) :: ipJE

    character(len=20) :: ens_cell_name

    integer(4)             :: iStep
    real(8),allocatable    :: times(:)

    real(8) :: dummy1, dummy2

    integer(4) :: iounit

    !--------------------------------------------------------------------------------------------------------------------------------



    write(*,'(A)') 'info: Starting WriteToENS'

!
!     INPUT ARGUMENTS
!

    Narg = command_argument_count()

    if ( Narg==1 ) then
        call get_command_argument(1,arg)
        if ( (trim(Upper_Case(arg)) == '-H') .or. (trim(Upper_Case(arg)) == '--HELP') ) then
            E_IO = Help()   ! show help section
        else
            write(*,*) 'Inconsistent arguments'
            write(*,*) '### Exit with error ###'
            stop
        end if
        stop
    end if

    !----------------------- WELCOME ----------------------------
    E_IO = Welcome()
    !------------------------------------------------------------

    if (Narg==3) then

        call get_command_argument(1,arg)
        read(arg,'(I10)') initial_count

        call get_command_argument(2,arg)
        read(arg,'(I10)') final_count

        call get_command_argument(3,arg)
        read(arg,'(I10)') step

        printGlobarIter = .FALSE.

    else
        write(*,*) 'Inconsistent arguments'
        write(*,*) '### Exit with error ###'
        stop
    end if


    !================================================================
    ! get free units
    Unit_MESH     = GetUnit()
    open(Unit_MESH     , FILE = 'Mesh.txt')
    Unit_BASPARAM = GetUnit()
    open(Unit_BASPARAM , FILE = 'Basparam.txt')
    ! for security purposes
    rewind(Unit_MESH)
    rewind(Unit_BASPARAM)
    !================================================================



    !
    ! Read Mesh
    !

    iError = iFindStringInFile('*NODAL DOFs',Unit_MESH)
    read(Unit_MESH,*) iDofT


    iError = iFindStringInFile('*DIMEN',Unit_MESH)
    read(Unit_MESH,*) Ndim

    iError = iFindStringInFile('*COORDINATES',Unit_MESH)
    read(Unit_MESH,*) NodT

    Allocate(x_uns(NodT),  &
             y_uns(NodT),  &
             z_uns(NodT)    )

    select case (Ndim)
        case(2)
            read(Unit_MESH,*) (x_uns(i), y_uns(i), i=1,NodT)
            z_uns = 0.d0
        case(3)
            read(Unit_MESH,*) (x_uns(i), y_uns(i), z_uns(i), i=1,NodT)
    end select

    iError = iFindStringInFile('*ELEMENT GROUPS',Unit_MESH)
    read(Unit_MESH,*) NGROUPS

    allocate(IDGroup(NGROUPS),  &
        NelTG(NGROUPS),    &
        NelName(NGROUPS),  &
        NodElG(NGROUPS)      )

    read(Unit_MESH,*) (IDGroup(i), NelTG(i), NelName(i), i=1,NGROUPS)

    allocate(IE(sum(NelTG)))

    If ( Upper_Case( NelName(1)(1:7) ) == 'GENERIC' ) then
        read(Unit_MESH,*) IE
    else
        write(*,*) 'ELEMENT TYPE NOT IMPLEMENTED'
        stop
    end if

    allocate(JE(sum(IE)))
    iError = iFindStringInFile('*INCIDENCE',Unit_MESH)
    read(Unit_MESH,*) JE

    ! Not needed for Ensight format
    !!JE = JE - 1   ! connectivity in VTK format include zero in numbering
    !!              ! so numbering is modidified




    !
    ! First naive implementation
    !
    E_IO = readMeshInfo()



    allocate(DOFNames(iDofT))

    ! reading DOF names
    ! search for Version 3 tag, if not use Version 2 tag
    rewind(Unit_BASPARAM)
    E_IO = iFindStringInFile('*NodalWriteToVTK',Unit_BASPARAM)
    IF (E_IO.eq.0) THEN
        read(Unit_BASPARAM,*) ( DOFNames(i) , i=1 , iDofT )
    ELSE
        rewind(Unit_BASPARAM)
        iError = iFindStringInFile('*StepContinuationControl',Unit_BASPARAM)
        read(Unit_BASPARAM,*)
        read(Unit_BASPARAM,*)
        read(Unit_BASPARAM,*)
        read(Unit_BASPARAM,*) ( DOFNames(i) , i=1 , iDofT )
    END IF


!
!     Busco grados de libertad de caracter escalar y vetorial
!
    write(*,'(A)') 'info: looking for scalar and vectorial DOFs'
    allocate(PointerDOFs(iDofT), PointerDOFv(iDofT))

    DOFv = 0        ! number of vectorial-DOF
    DOFs = 0        ! number of scalar-DOF
    j = 0
    k = 0
    do i=1, iDofT

        if (DOFNames(i)(1:1) == '!') cycle

        LastChar = len(trim(DOFNAMES(i)))  ! DOF Name Lenght
        !    if (.true. .or. .false.) then
        if ( Upper_Case(DOFNAMES(i)(LastChar:LastChar)) == 'X' .or. DOFNames(i)(LastChar:LastChar) == '1' .or. &
            Upper_Case(DOFNAMES(i)(LastChar:LastChar)) == 'Y' .or. DOFNames(i)(LastChar:LastChar) == '2' .or. &
            Upper_Case(DOFNAMES(i)(LastChar:LastChar)) == 'Z' .or. DOFNames(i)(LastChar:LastChar) == '3' ) then

            DOFv = DOFv + 1
            j = j + 1
            PointerDOFv(j) = i
        else
            DOFs = DOFs + 1
            k = k + 1
            PointerDOFs(k) = i
        end if
    end do

    ! write summay
    write(*,'(A)') 'summary:'
    write(*,'(A,X,I3)') 'Scalar DOFs:',DOFs
    do j=1, DOFs
        FieldName = TRIM(DOFNAMES(PointerDOFs(j)))
        write(*,'(A)') FieldName
    enddo

    write(*,'(A,X,I3)') 'Vector DOFs:',DOFv
    do j=1, DOFv, Ndim
        LastChar = LEN(TRIM(DOFNAMES(PointerDOFv(j))))
        FieldName = TRIM(DOFNAMES(PointerDOFv(j)))
        write(*,'(A)') FieldName(1:LastChar-1)
    enddo



!
!     Write GEO file
!
    write(*,'(A)',advance='no') 'info: writing geo file...'
    iError = ENS_INI_GEO('mesh.geo')
    iError = ENS_GEO_UNST(NodT,x_uns,y_uns,z_uns)
    ipJE = 0
    do i=1, nPhyGroups
        if (phyGroupName(i)(1:1).eq.'!') then
            ipJE = ipJE + phyGroupEl(i)*phyGroupNode(i)    ! # elements * # nodes-each-element
            cycle
        endif


        call gmsh2ens(phyGroupType(i),ens_cell_name)

        iError = ENS_CON(NC    = phyGroupEl(i) ,                  &
                         NodEl = phyGroupNode(i) ,                 &
                         NodG  = select_nodel   (phyGroupType(i)) ,  &
                         JE    = JE(ipJE+1),                      &
                         groupType = TRIM(ens_cell_name)  )

        ipJE = ipJE + phyGroupEl(i)*phyGroupNode(i)    ! # elements * # nodes-each-element

    enddo

    iError = ENS_END_GEO()
    write(*,'(A)') 'OK'



    allocate(solution(NodT,iDofT))


    i = CEILING((final_count-initial_count)/FLOAT(step)) + 1
    allocate(times(i))

    iStep = 0
    do i=initial_count, final_count, step


!
!     Read Data from DataOutN-%d08.txt
!

        write(ArchiveNameINPUT,'(1A9,1I8.8,1A4)')"DataOutN-",i,".txt"
        Unit_DataOUT = GetUnit()
        open(Unit_DataOUT, FILE = ArchiveNameINPUT)
        iError = iFindStringInFile('*Time',Unit_DataOUT)
        read(Unit_DataOUT,*) dummy1, times(iStep+1), dummy2
        read(Unit_DataOUT,*) (solution(j,:), j=1, NodT)
        close(Unit_DataOUT)
!
!    Healing data. As the current format is Exponential form E+00
!

    where (solution.GE.1.0d100)
        solution = 0.99999d+99
    endwhere
    where (solution.LE.-1.0d100)
        solution = -0.99999d+99
    endwhere
    where (DABS(solution).LE.1.0d-100)
        solution = 0.0d0
    endwhere


!
!     print scalar DOF
!
        do j=1, DOFs
            FieldName = TRIM(DOFNAMES(PointerDOFs(j)))
            write(ArchiveNameOUPUT,'(A,1I8.8,A)') TRIM(FieldName)//'-',iStep,".scl"
            iError = ENS_VAR_SCAL(NC_NN   = NodT,                       &
                                  varname = FieldName,                  &
                                  var     = solution(:,PointerDOFs(j)), &
                                  filename= 'ensightData/'//ArchiveNameOUPUT)
        enddo

!
!     print vectorial DOF
!

        do j=1, DOFv, Ndim
            LastChar = LEN(TRIM(DOFNAMES(PointerDOFv(j))))
            FieldName = TRIM(DOFNAMES(PointerDOFv(j)))
            write(ArchiveNameOUPUT,'(A,1I8.8,A)') TRIM(FieldName(1:LastChar-1))//'-',iStep,".vec"
            select case(Ndim)
                case(2)
                    iError = ENS_VAR_VEC(NC_NN    = NodT,                         &
                                         varname  = FieldName(1:LastChar-1),      &
                                         varX     = solution(:,PointerDOFv(j)),   &
                                         varY     = solution(:,PointerDOFv(j+1)), &
                                         varZ     = (/(0.0d0,k=1,NodT)/),         &
                                         filename = 'ensightData/'//ArchiveNameOUPUT)
                case(3)
                    iError = ENS_VAR_VEC(NC_NN    = NodT,                         &
                                         varname  = FieldName(1:LastChar-1),      &
                                         varX     = solution(:,PointerDOFv(j)),   &
                                         varY     = solution(:,PointerDOFv(j+1)), &
                                         varZ     = solution(:,PointerDOFv(j+2)), &
                                         filename = 'ensightData/'//ArchiveNameOUPUT)
            end select
        enddo

        iStep = iStep + 1

    enddo

!
! generate case file
!

    write(*,'(A)') 'info: writing .case file'

    iounit = GetUnit()
    open(unit   = iounit,           &
         file     = 'gpsolver.case',&
         form     = 'FORMATTED',    &
         access   = 'SEQUENTIAL',   &
         action   = 'WRITE',        &
         iostat   = iError)

    write(iounit,'(A)',iostat=iError) 'FORMAT'
    write(iounit,'(A)',iostat=iError) 'type: ensight gold'

    write(iounit,'(A)',iostat=iError) 'GEOMETRY'
    write(iounit,'(A)',iostat=iError) 'model: ensightData/mesh.geo'

    write(iounit,'(A)',iostat=iError) 'VARIABLE'
    do j=1, DOFs
        FieldName = TRIM(DOFNAMES(PointerDOFs(j)))
        write(ArchiveNameOUPUT,'(A)') TRIM(FieldName)//'-********.scl'
        write(iounit,'(A,X,A30,X,A)',iostat=iError) 'scalar per node:',FieldName,'ensightData/'//ArchiveNameOUPUT
    enddo
    do j=1, DOFv, nDim
        LastChar = LEN(TRIM(DOFNAMES(PointerDOFv(j))))
        FieldName = TRIM(DOFNAMES(PointerDOFv(j)))
        FieldName = FieldName(1:LastChar-1)
        write(ArchiveNameOUPUT,'(A)') TRIM(FieldName(1:LastChar-1))//'-********.vec'
        write(iounit,'(A,X,A30,X,A)',iostat=iError) 'vector per node:',FieldName,'ensightData/'//ArchiveNameOUPUT
    enddo

    write(iounit,'(A)',iostat=iError)       'TIME'
    write(iounit,'(A)',iostat=iError)       'time set: 1'
    write(iounit,'(A,X,I10)',iostat=iError) 'number of steps:', iStep
    write(iounit,'(A)',iostat=iError)       'filename start number: 0'
    write(iounit,'(A)',iostat=iError)       'filename increment: 1'
    write(iounit,'(A)',iostat=iError)       'time values:'
    write(iounit,'(E12.5)',iostat=iError) times(1:iStep)

    close(iounit)

    write(*,'(A)') 'info: Complete'


    CONTAINS

    function Welcome() result(E_IO)
    integer(4) E_IO

    write(*,*) 'WriteToENS: gpsolver translator to ENSIGHT GOLD format.'
    Write(*,*) 'Version 1.0'
    Write(*,*) 'Created by NB'
    write(*,*) 'Input -h or --help for more details'

    end function

    function Help() result(E_IO)
        !------------------------------------------------------------------
        !
        ! Write's manual page on screen
        !
        !------------------------------------------------------------------
        integer(4) E_IO

        write(*,*) 'WriteToENS: Write to gpsolver output into ENSIGHT GOLD (7.6) format'

        write(*,*) '### How to call it'
        write(*,*) '$: WriteToENS $initial_step $final_step $step'
        write(*,*) '-------------------------------------------------------'
        write(*,*)
        write(*,*) 'DOF names are readed from *NodalWriteToVTK. In case of failure'
        write(*,*) 'to find this flag, it is look for *StepContinuationControl in '
        write(*,*) 'Basparam file.'
        write(*,*)
        write(*,*) 'Vectorial DOF are detected with x,y,z or 1,2,3 at the end of the DOF name'
        write(*,*) 'Plot all DOF by default. To plot in selective mode use "!"  '
        write(*,*) 'character at the begin of the DOF that you want to dismiss.'


    end function Help


end program WriteToENS

# WriteToENS

Post-processing utility for gpsolver output. 
Translates gpsolver files to Ensight Case Gold format. 

# How to use

```bash
WriteToENS initStep endStep iStep
```
where `initStep` is the initial step (int), `endStep` the end step (int) and
`iStep` is the increment between steps (int). 

Thereafter, `gpsolver.case` file and `ensightCase` folder are created. The
first is used as the entrypoint to Paraview. The `ensightCase` folder contains
all files related to geometry `.geo` (static) and data `.scl` and `.vec` files.


# Installation

There is a `makefile` in `build` folder. Files are linked there and then the
compilation and installation on system is carried out.

```bash
cd build/
make create.links
make all
make install
```
Therefore, the utility is avaiable as `WriteToENS` overall system.
The variable `INSTALL_DIR` is set to `~/bin` as default within `makefile` file.
Be sure `INSTALL_DIR` is contained in the `$PATH` 



# TODO

- Add writer by elements in a consistently manner (as in vtk format)

- ~~Add compilation instructions via makefile~~

- Make some order in the project  

- Code cleaning/refactoring. Remove unused chunks of code
